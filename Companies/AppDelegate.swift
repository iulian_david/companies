//
//  AppDelegate.swift
//  Companies
//
//  Created by iulian david on 28/03/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    fileprivate func setupDefaultNavigationBarDefaultAppearance() {
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = .lightRed
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        
        setupDefaultNavigationBarDefaultAppearance()
        
        
        
//        let companiesController = CompaniesController()
        let companiesController = CompaniesAutoUpdateController()
        let navController = UINavigationController(rootViewController: companiesController)
        window?.rootViewController = navController
        
        return true
    }

}

