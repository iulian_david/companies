//
//  EmployeeType.swift
//  Companies
//
//  Created by iulian david on 27/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation

enum EmployeeType : String, Comparable, CaseIterable, Codable {
    typealias RawValue = String
    
    static func < (lhs: EmployeeType, rhs: EmployeeType) -> Bool {
        return lhs.hashValue < rhs.hashValue
    }
    
    case executives = "Executives"
    case seniorManagement = "Senior Management"
    case staff = "Staff"
    case interns = "Interns"
    
    init?(rawValue: String) {
        switch rawValue.lowercased() {
        case "executives", "executive": self = .executives
        case "senior management", "senior": self = .seniorManagement
        case "staff": self = .staff
        case "interns": self = .interns
        default:
            return nil
        }
    }

}

#if !swift(>=4.2)
public protocol CaseIterable {
    associatedtype AllCases: Collection where AllCases.Element == Self
    static var allCases: AllCases { get }
}
extension CaseIterable where Self: Hashable {
    static var allCases: [Self] {
        return [Self](AnySequence { () -> AnyIterator<Self> in
            var raw = 0
            var first: Self?
            return AnyIterator {
                let current = withUnsafeBytes(of: &raw) { $0.load(as: Self.self) }
                if raw == 0 {
                    first = current
                } else if current == first {
                    return nil
                }
                raw += 1
                return current
            }
        })
    }
}
#endif
