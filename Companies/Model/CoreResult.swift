//
//  CoreResult.swift
//  Companies
//
//  Created by iulian david on 20/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation

enum CoreDataResult<Value> {
    case success(Value)
    case failure(Error)
}
