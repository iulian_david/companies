//
//  CustomConvertV1.1To2.swift
//  Companies
//
//  Created by iulian david on 28/07/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import CoreData

class CustomConvertV1_1To1_2: NSEntityMigrationPolicy {
    
    /// Mapping function for numOfEmployees from Int16(CoreData stores it as NSNumber) to String
    /// see: ModelMappingv1.1..2 -> CompanyToCompany->Custom Policy
    /// where numOfEmployee "Value Expression" is FUNCTION($entityPolicy, "transformNumOfEmployees:" , $source.numOfEmployees)
    /// - Parameter number: integer value from DB
    /// - Returns: String calculation based on integer value
    @objc func transformNumOfEmployees(_ number: NSNumber) -> String {
        if number.intValue > 100 {
            return "small"
        } else {
            return "large"
        }
    }
}
