//
//  JSONCompany.swift
//  Companies
//
//  Created by iulian david on 28/07/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation

struct JSONCompany: Codable {
    let name: String
    let photoUrl: String?
    let founded: String
    
    let employees: [JSONEmployee]?
}

struct JSONEmployee: Codable {
    let name: String
    let type: EmployeeType
    let birthday: String
}
