//
//  UIView+Extensions.swift
//  Companies
//
//  Created by iulian david on 13/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setupPlusButtonInNavBar(selector: Selector) {
       navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "plus"), style: .plain, target: self, action: selector)
    }
    
    func setupCancelButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelModel))
    }
    
    @objc func handleCancelModel() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupLightBlueBackgroundView(height: CGFloat) -> UIView {
        let lightBackgroundView = UIView()
        lightBackgroundView.backgroundColor = .lightBlue
        lightBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lightBackgroundView)
        
        lightBackgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        lightBackgroundView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        lightBackgroundView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        lightBackgroundView.heightAnchor.constraint(equalToConstant: height).isActive = true
        return lightBackgroundView
    }
    
    func showAlertError(error: Error) {
        let alertAction = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertAction.addAction(cancelAction)
        present(alertAction, animated: true, completion: nil)
    }
    
    func showAlertError(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
