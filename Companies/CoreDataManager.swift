//
//  CoreDataManager.swift
//  Companies
//
//  Created by iulian david on 03/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import CoreData

struct CoreDataManager {
    static let shared = CoreDataManager()
    
    let presistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CompaniesModel")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err {
                fatalError("Loading of store failed \(err)")
            }
        }
        return container
    }()
    
    func fetchCompanies() -> [Company] {
        let context = presistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        do {
            let companies = try context.fetch(fetchRequest)
            return companies
        } catch let fetchErr {
            print("Error when fetching companies \(fetchErr.localizedDescription)")
            return []
        }
    }
    
    func removeAllCompanies() {
        let context = presistentContainer.viewContext
        let fetch: NSFetchRequest<NSFetchRequestResult> = Company.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            try context.execute(deleteRequest)
        } catch let delErr {
            print("error when deleting company", delErr)
        }
    }
    
    func createCompany(fromName name: String, founded: Date, imageData: Data?, completion: @escaping (CoreDataResult<Company>) -> Void) {
        let context = presistentContainer.viewContext
        let company =  Company.init(context: context)
        company.name = name
        company.founded = founded
        company.imageData = imageData
    
        do {
            //perform the save
            try context.save()
            completion(.success(company))
        } catch let saveErr {
            print("Failed to save company \(saveErr)")
            completion(.failure(saveErr))
        }
        
    }
    
    func createEmployee(company: Company, name: String, birthDay: Date?, employeeType: String, completion: @escaping (CoreDataResult<Employee>) -> Void) {
        let context = presistentContainer.viewContext
        let employee =  Employee.init(context: context)
        employee.fullName = name
        employee.company = company
        employee.type = employeeType
        
        let employeeInformation = EmployeeInformation.init(context: context)
        employeeInformation.birthday = birthDay
        employeeInformation.taxid = "456"
        
        employee.information = employeeInformation
        do {
            //perform the save
            try context.save()
            completion(.success(employee))
        } catch let saveErr {
            print("Failed to save employee \(saveErr)")
            completion(.failure(saveErr))
        }
    }
    
    func updateCompany(company: Company) {
        let context = presistentContainer.viewContext
        do {
            //perform the save
            try context.save()
        } catch let saveErr {
            print("Failed to save company \(saveErr)")
        }
    }
}
