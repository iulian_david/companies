//
//  CompanyCell.swift
//  Companies
//
//  Created by iulian david on 13/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    
    let companyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        imageView.layer.borderColor = UIColor.darkBlue.cgColor
        imageView.layer.borderWidth = 2
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameFoundLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    var company:Company? {
        didSet {
            updateCell()
        }
    }
    
    fileprivate func updateCell() {
        let companyName = company?.name ?? ""
        let date:String
        if company?.founded == nil {
            date = ""
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM dd, yyyy"
            dateFormatter.dateStyle = .long
            
            let founded = dateFormatter.string(from: company!.founded!)
            date = company!.founded == nil ? "" : " - Founded: \(founded)"
        }
        
        nameFoundLabel.text = "\(companyName)\(date)"
        
        if let imageData = company?.imageData {
            companyImageView.image = UIImage.init(data: imageData)
        } else {
            companyImageView.image = #imageLiteral(resourceName: "select_photo_empty")
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .tealColor
        
        addSubview(companyImageView)
        
        
        companyImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        companyImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        companyImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        companyImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(nameFoundLabel)
        
        nameFoundLabel.leftAnchor.constraint(equalTo: companyImageView.rightAnchor, constant: 10).isActive = true
        nameFoundLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        nameFoundLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        nameFoundLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
