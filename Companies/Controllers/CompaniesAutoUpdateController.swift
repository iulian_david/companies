//
//  CompaniesAutoUpdateController.swift
//  Companies
//
//  Created by iulian david on 27/07/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//
import UIKit
import CoreData

class CompaniesAutoUpdateController: UITableViewController {
    
    let cellId = "cellId"
    
    lazy var fetchedResultsController: NSFetchedResultsController<Company> = {
        let request: NSFetchRequest<Company> = Company.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: "name", ascending: true)
        ]
        let context: NSManagedObjectContext = CoreDataManager.shared.presistentContainer.viewContext
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: "name", cacheName: nil)
        frc.delegate = self
        do {
            try frc.performFetch()
        } catch let fetchErr {
            print(fetchErr
            )
        }
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleLabel = UILabel()
        titleLabel.text = NSLocalizedString("main_name", comment: "Company Auto Updates")
        titleLabel.adjustsFontSizeToFitWidth = true
        navigationItem.titleView = titleLabel
        navigationItem.largeTitleDisplayMode = .always
        tableView.register(CompanyCell.self, forCellReuseIdentifier: cellId)
        tableView.backgroundColor = .darkBlue
        
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(handleAdd)),
             UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(handleDelete))
        ]
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        refreshControl.tintColor = .white
        self.refreshControl = refreshControl
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = IndentedLabel()
        label.text = fetchedResultsController.sectionIndexTitles[section]
        label.backgroundColor = .lightBlue
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CompanyCell
        let company = fetchedResultsController.object(at: indexPath)
        cell.company = company
            
        return cell
    }
    
    @objc private func handleAdd() {
        let context = CoreDataManager.shared.presistentContainer.viewContext
        let company = Company(context: context)
        company.name = "ZZZ"
        do {
            try context.save()
        } catch let saveErr {
            print("Unable to save compay \(saveErr)")
        }
    }
    
    @objc private func handleDelete() {
        let context = CoreDataManager.shared.presistentContainer.viewContext
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
//        fetchRequest.predicate = NSPredicate(format: "name contains %@", "B")
        do {
            let companysWithB = try context.fetch(fetchRequest)
            print(companysWithB)
            companysWithB.forEach{ context.delete($0) }
            try context.save()
        } catch let fetchErr {
            print("Unable to fetch companies \(fetchErr)")
        }
    }
    
    @objc private func handleRefresh() {
        Service.shared.getCompanies()
        self.refreshControl?.endRefreshing()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let employeesController = EmployeesController()
        employeesController.company = fetchedResultsController.object(at: indexPath)
        navigationController?.pushViewController(employeesController, animated: true)
    }
}

extension CompaniesAutoUpdateController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
}
