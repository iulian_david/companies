//
//  ViewController.swift
//  Companies
//
//  Created by iulian david on 28/03/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit
import CoreData


class CompaniesController: UITableViewController {
    
    var companies = [Company]()
    let cellId = "cellId"

    @objc private func doWork() {
        print("Trying to do work...")
        
        //creating some Company objects on a background thread
        
        CoreDataManager.shared.presistentContainer.performBackgroundTask { (backgroundContext) in
            (0...5).forEach { (value) in
                print(value)
                let company = Company(context: backgroundContext)
                company.name = String(value)
                
            }
            do {
                try backgroundContext.save()
                DispatchQueue.main.async {
                    self.fetchCompanies()
                }
            } catch let err {
                print("Fail to save ", err)
            }
        }
        
    }
    
    // try to perform updates
    @objc private func doUpdate() {
        CoreDataManager.shared.presistentContainer.performBackgroundTask { (backgroundContext) in
            let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
            var companies = [Company]()
            do {
                companies = try backgroundContext.fetch(fetchRequest)
            } catch let findErr {
                print("unable to get all companies ", findErr)
            }
            companies.forEach({ (company) in
                company.name = "B \(company.name ?? "")"
            })
            do {
                try backgroundContext.save()
                DispatchQueue.main.async {
                    CoreDataManager.shared.presistentContainer.viewContext.reset()
                    self.fetchCompanies()
                }
            } catch let saveErr {
                print("Unable to save ", saveErr)
            }
            
        }
    }
    
    @objc private func doNestedUpdates() {
        DispatchQueue.global(qos: .background).async {
            // we'll try to do updates
            
            // first construct a custom context
            let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            
            privateContext.parent = CoreDataManager.shared.presistentContainer.viewContext
            
            let request: NSFetchRequest<Company> = Company.fetchRequest()
            request.fetchLimit = 1
            request.fetchOffset = 2
            do {
                
                let companies = try privateContext.fetch(request)
                companies.forEach({ (company) in
                    company.name = "D: \(company.name ?? "")"
                })
                
                do {
                    try privateContext.save()
                    // after save success
                    DispatchQueue.main.async {
                        do {
                            let context = CoreDataManager.shared.presistentContainer.viewContext
                            if context.hasChanges {
                                try context.save()
                            }
                        } catch let finalSaveErr {
                            print("Failed to save main context ", finalSaveErr)
                        }
                        self.tableView.reloadData()
                    }
                } catch let saveErr {
                    print("failed to save on private context, ", saveErr)
                }
            } catch let fetchErr {
                print("Failed to fetch request on private context ", fetchErr)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        navigationItem.title = "Companies"
        
        tableView.backgroundColor = .darkBlue
        tableView.separatorColor = .white
        tableView.register(CompanyCell.self, forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()// blank UIView to not show separator for not available rows
        
        setupPlusButtonInNavBar(selector: #selector(handleAddCompany))
        
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(handleReset)),
            UIBarButtonItem(title: "Do Work", style: .plain, target: self, action: #selector(doWork)),
            UIBarButtonItem(title: "Nested Updates", style: .plain, target: self, action: #selector(doNestedUpdates))
        ]
        fetchCompanies()
        
    }

    private func fetchCompanies() {
        companies = CoreDataManager.shared.fetchCompanies()
        tableView.reloadData()
    }
    
    @objc private func handleReset() {
        CoreDataManager.shared.removeAllCompanies()
        
        var indexPathsToRemove = [IndexPath]()
        for (index, _) in companies.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            indexPathsToRemove.append(indexPath)
        }
        companies.removeAll()
        tableView.deleteRows(at: indexPathsToRemove, with: .left)
    }
    
    @objc func handleAddCompany() {
        let createCompanyController = CreateCompanyViewController()
        createCompanyController.delegate = self
        let navController = UINavigationController(rootViewController: createCompanyController)
        present(navController, animated: true, completion: nil)
    }
}


