//
//  EmployeesController+UITableViewDelegate.swift
//  Companies
//
//  Created by iulian david on 20/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit


class IndentedLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsetsMake(0, 16, 0, 0)
        let customRect = UIEdgeInsetsInsetRect(rect, insets)
        super.drawText(in: customRect)
    }
}

extension EmployeesController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allEmployees[sections[section]]?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let key = sections[indexPath.section]
        guard let employee = allEmployees[key]?[indexPath.row] else {
                fatalError("No employee found")
        }
        
        cell.textLabel?.text = employee.fullName
        
        if let taxId = employee.information?.taxid {
            cell.textLabel?.text = "\(employee.fullName!) \(taxId)"
        }
        
        if let birthDate = employee.information?.birthday, let alreadyText = cell.textLabel?.text {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM dd, yyyy"
            dateFormatter.dateStyle = .long
            cell.textLabel?.text = "\(alreadyText) \(dateFormatter.string(from: birthDate))"
        }
        cell.backgroundColor = .tealColor
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = IndentedLabel()
        label.text = sections[section].rawValue
       
        label.backgroundColor = .lightBlue
        label.textColor = .darkBlue
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
