//
//  EmployeesController+CreateEmployee.swift
//  Companies
//
//  Created by iulian david on 20/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation


protocol CreateEmployeeControllerDelegate {
    func didAddEmployee(employee: Employee)
    func didEditEmployee(employee: Employee)
}

extension EmployeesController: CreateEmployeeControllerDelegate {
    
    func didAddEmployee(employee: Employee) {
        guard let empTypeString = employee.type, let empType = EmployeeType(rawValue: empTypeString)  else { return }
        
        
        allEmployees[empType]?.append(employee)
        
        // If no section was created
        guard  let section = sections.index(of: empType), let employeeCount = allEmployees[empType]?.count else {
            let newSection = tableView.numberOfSections
            tableView.insertSections([newSection], with: .middle)
            return
        }
        tableView.insertRows(at: [IndexPath(row: employeeCount - 1, section: section)], with: .automatic)
    }
    
    func didEditEmployee(employee: Employee) {
        guard let row = employees.index(of: employee) else {
            return
        }
        let reloadIndexPath = IndexPath(item: row, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .middle)
    }
}
