//
//  EmployeesController.swift
//  Companies
//
//  Created by iulian david on 13/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit


class EmployeesController: UITableViewController {
    
    var company: Company?
    var employees = [Employee]()
    var allEmployees = [EmployeeType : [Employee]]()
    
    let sections = EmployeeType.allCases
    let cellId = "cellId"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "\(company?.name ?? "") \(company?.numOfEmployees ?? "")"
        
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.backgroundColor = .darkBlue
        tableView.separatorColor = .white
        setupPlusButtonInNavBar(selector: #selector(handleAdd))
        fetchEmployees()
    }
    
    @objc private func handleAdd() {
        print("Trying to add")
        guard let company = company else { return  }
        let createEmployeeController = CreateEmployeeController(company: company)
        createEmployeeController.delegate = self
        let navController = UINavigationController(rootViewController: createEmployeeController)
        present(navController, animated: true, completion: nil)
    }
    
 
    
    func fetchEmployees() {
        guard let company = company, let companyEmployees = company.employees?.allObjects as? [Employee]  else { return  }
        
        sections.forEach { (employeeType) in
            if allEmployees[employeeType] == nil {
                allEmployees[employeeType] = []
            }
            
        }
        
        companyEmployees.forEach { employee in
            if let empTypeString = employee.type,
                let employeeType: EmployeeType = EmployeeType(rawValue: empTypeString) {
               allEmployees[employeeType]?.append(employee)
            }
        }
    }
}
