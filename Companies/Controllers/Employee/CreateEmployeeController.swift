//
//  CreateEmployyeController.swift
//  Companies
//
//  Created by iulian david on 13/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class CreateEmployeeController : UIViewController {
    
    var delegate: CreateEmployeeControllerDelegate?
    var company: Company
    private static let df = DateFormatter()
    private static let formatter = "MM/dd/yyyy"
    var employeeTypes: [String] = []
    
    init(company: Company) {
        
        self.company = company
        super.init(nibName: nil, bundle: nil)
        CreateEmployeeController.df.dateFormat = CreateEmployeeController.formatter
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        //enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let birthdayLabel: UILabel = {
        let label = UILabel()
        label.text = "Birthday"
        //enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let birthdayField: UITextField = {
        let textField = UITextField()
        textField.placeholder = formatter
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    lazy var segments: UISegmentedControl = {
        let segment = UISegmentedControl(items: employeeTypes)
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.tintColor = .darkBlue
        return segment
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .darkBlue
        navigationItem.title = "Create Employee"
        
        setupCancelButton()
        
        setupUI()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
    }
    
    private func setupUI() {
        _ = setupLightBlueBackgroundView(height: 160)
        view.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(nameTextField)
        nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        
        view.addSubview(birthdayLabel)
        birthdayLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        birthdayLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        birthdayLabel.widthAnchor.constraint(equalTo: nameLabel.widthAnchor).isActive = true
        birthdayLabel.heightAnchor.constraint(equalTo: nameLabel.heightAnchor).isActive = true
        
        view.addSubview(birthdayField)
        birthdayField.topAnchor.constraint(equalTo: birthdayLabel.topAnchor).isActive = true
        birthdayField.leftAnchor.constraint(equalTo: birthdayLabel.rightAnchor).isActive = true
        birthdayField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        birthdayField.bottomAnchor.constraint(equalTo: birthdayLabel.bottomAnchor).isActive = true
        
        employeeTypes = EmployeeType.allCases.map{$0.rawValue}
        
        
        view.addSubview(segments)
        segments.selectedSegmentIndex = 0
        segments.topAnchor.constraint(equalTo: birthdayLabel.bottomAnchor, constant: 0).isActive = true
        segments.leftAnchor.constraint(equalTo: birthdayLabel.leftAnchor).isActive = true
        segments.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        segments.heightAnchor.constraint(equalToConstant: 34).isActive = true
        

    }
    
    @objc private func handleSave() {
        guard let name = nameTextField.text, !name.isEmpty else {
            showAlertError(title: "Empty Name", message: "You have not entered a name")
            return
        }
        
        guard let birthDayText = birthdayField.text, !birthDayText.isEmpty else {
            showAlertError(title: "Empty birthday", message: "You have not entered a birthday")
            return
        }
       
        guard let birthday = CreateEmployeeController.df.date(from: birthDayText) else {
            showAlertError(title: "Bad date", message: "Birthday data entered not valid")
            return
        }
        guard let empType = segments.titleForSegment(at: segments.selectedSegmentIndex) else { return }
        
        // turn birthdayTextField.text
        CoreDataManager.shared.createEmployee(company: company, name: name, birthDay: birthday, employeeType: empType) { [weak self] (result) in
            switch result {
            case .success(let employee):
                self?.dismiss(animated: true,completion: {
                    self?.delegate?.didAddEmployee(employee: employee)
                })
            case .failure(let err) :
                self?.showAlertError(error: err)
            }
        }
        
       
    }
}
