//
//  CreateCompanyViewController.swift
//  Companies
//
//  Created by iulian david on 03/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit
import CoreData

enum EntityErrors: Error {
    case entityCreated
}

class CreateCompanyViewController: UIViewController {
    
    var delegate: CreateCompanyControllerDelegate?
    
    fileprivate func setupCircularImageStyle() {
        companyImageView.layer.cornerRadius = companyImageView.frame.width/2
        companyImageView.clipsToBounds = true
        companyImageView.layer.borderColor = UIColor.darkBlue.cgColor
        companyImageView.layer.borderWidth = 2
    }
    
    var company: Company? {
        didSet {
            nameTextField.text = company?.name
            datePicker.date = company?.founded ?? Date()
            if let imageData = company?.imageData {
                companyImageView.image = UIImage(data: imageData)
                setupCircularImageStyle()
            }
        }
    }
    
    lazy var companyImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "select_photo_empty"))
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true // to alow action on it
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectPhoto)))
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        //enable autolayout
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let datePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = .date
        dp.translatesAutoresizingMaskIntoConstraints = false
        return dp
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = company == nil ? "Create Company" : "Edit Company"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        setupCancelButton()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(handleSave))
        view.backgroundColor = .darkBlue
        
    }
    
    @objc private func handleSelectPhoto() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
   
    @objc func handleSave() {
        
        if company == nil {
            createCompany()
        } else {
            saveCompanyChanges()
        }
        
    }
    
    private func saveCompanyChanges() {
        guard let name = self.nameTextField.text, let company = company else {
            return
        }

        company.name = name
        company.founded = datePicker.date
        if let companyImage = companyImageView.image, let imageData = UIImageJPEGRepresentation(companyImage, 0.8) {
            company.imageData = imageData
        }
        CoreDataManager.shared.updateCompany(company: company)
        dismiss(animated: true,completion: { [unowned self] in
            self.delegate?.didEditCompany(company: company)
        })
        
    }
    
    private func createCompany() {
       
        guard let name = self.nameTextField.text else {
            return
        }
        var imageData: Data?
        if let companyImage = companyImageView.image, let imagData = UIImageJPEGRepresentation(companyImage, 0.8) {
            imageData = imagData
        }
        
        CoreDataManager.shared.createCompany(fromName: name, founded: datePicker.date, imageData: imageData) { [weak self] (result) in
            switch result {
            case .success(let companyObj) :
                self?.dismiss(animated: true,completion: {
                    self?.delegate?.didAddCompany(company: companyObj)
                })
            case .failure(let error):
                self?.showAlertError(error: error)
            }
        }
        
       
    }
    
    private func setupUI() {
       let lightBackgroundView = setupLightBlueBackgroundView(height: 350)
        
        view.addSubview(companyImageView)
        companyImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
        companyImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        companyImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        view.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: companyImageView.bottomAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(nameTextField)
        nameTextField.topAnchor.constraint(equalTo: nameLabel.topAnchor).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameTextField.bottomAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        
        //setup the datepicker
        
        view.addSubview(datePicker)
        datePicker.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        datePicker.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        datePicker.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: lightBackgroundView.bottomAnchor).isActive = true
        
    }
}

extension CreateCompanyViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
           companyImageView.image = editedImage
        } else if let original = info[UIImagePickerControllerOriginalImage] as? UIImage {
           companyImageView.image = original
        }
        setupCircularImageStyle()
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
