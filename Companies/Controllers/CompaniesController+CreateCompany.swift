//
//  CompaniesController+CreateCompany.swift
//  Companies
//
//  Created by iulian david on 13/04/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation

protocol CreateCompanyControllerDelegate {
    func didAddCompany(company: Company)
    func didEditCompany(company: Company)
}


extension CompaniesController: CreateCompanyControllerDelegate {
    
    func didAddCompany(company: Company) {
        // 1 - modify the companies array
        companies.append(company)
        //2 - insert a new index path into tableView
        let indexPath = IndexPath(row: companies.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func didEditCompany(company: Company) {
        guard let row = companies.index(of: company) else {
            return
        }
        let reloadIndexPath = IndexPath(item: row, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .middle)
    }
}
