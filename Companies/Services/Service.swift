//
//  Service.swift
//  Companies
//
//  Created by iulian david on 28/07/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import Foundation
import CoreData

struct Service {
    static let shared = Service()
    
    public func getCompanies() {
        guard let url = URL(string: "https://api.letsbuildthatapp.com/intermediate_training/companies") else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Cannot retrieve data from url", err)
                return
            }
            guard let data = data else {
                print("Cannot retrieve data from url", url)
                return
            }
            print(NSLocalizedString("Request was successful", comment: "Request was successful"))
            let jsonDecoder = JSONDecoder()
            let companies: [JSONCompany]
            do {
                companies = try jsonDecoder.decode([JSONCompany].self, from: data)
                let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                privateContext.parent = CoreDataManager.shared.presistentContainer.viewContext
                companies.forEach({ jsonCompany in
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    let company = Company(context: privateContext)
                    company.name = jsonCompany.name
                    if !jsonCompany.founded.isEmpty {
                        let founded = jsonCompany.founded
                        company.founded = dateFormatter.date(from: founded)
                    }
                    //photo retrieval
                    
                    jsonCompany.employees?.forEach( { jsonEmp in
                       let employee = Employee(context: privateContext)
                        employee.fullName = jsonEmp.name
                        employee.type = jsonEmp.type.rawValue
                        employee.company = company
                        let employeeInfo = EmployeeInformation(context: privateContext)
                        employeeInfo.birthday = dateFormatter.date(from: jsonEmp.birthday)
                        employee.information = employeeInfo
                    })
                })
                do {
                    try privateContext.save()
                    try privateContext.parent?.save()
                } catch let saveErr {
                    print(NSLocalizedString("fail_cd_company",comment: "Error on saving company to CoreData"), saveErr)
                }
                
                
            } catch let decodeErr {
                print("Cannot get companies from retrieved data", decodeErr)
                return
            }
        }.resume()
    }
}
